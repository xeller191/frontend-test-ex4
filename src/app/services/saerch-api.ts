import axios from 'axios'
import { SearchApiResponse } from '../types/saerch-api-response'

export async function checkContractOverlap(
  searchQuery: string
): Promise<SearchApiResponse[]> {
  try {
    const response = await axios.get(
      `http://localhost:3000/api/place-search?search=${searchQuery}`
    )
    return response.data.result
  } catch (error: any) {
    throw new Error(error)
  }
}
