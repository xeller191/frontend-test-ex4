'use client'
import Content from './components/content'
import Footer from './components/footer'
import Header from './components/header'

export default function Home() {
  return (
    <>
      <div className="max-w-[90%] md:max-w-[80%] mx-auto min-w-[450px]">
        <Header />
        <div className="h-4" />
        <div className=" md:min-h-[calc(100vh-200px)] min-h-[calc(100vh-260px)]">
          <Content />
        </div>
      </div>
      <Footer />
    </>
  )
}
