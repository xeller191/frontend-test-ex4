import { useCallback, useEffect, useState } from 'react'
import { useAppSelector } from '../lib/hook'
import { checkContractOverlap } from '../services/saerch-api'
import { SearchApiResponse } from '../types/saerch-api-response'

function useContent() {
  const [resultSearchData, setResultSearchData] = useState<SearchApiResponse[]>(
    []
  )
  const { search } = useAppSelector((state) => state.searchStore)
  console.log({ search })

  const searchApi = useCallback(async () => {
    try {
      const searchData = await checkContractOverlap(search)
      setResultSearchData(searchData)
    } catch (error) {
      console.error(error)
    }
  }, [search])

  useEffect(() => {
    if (search) {
      searchApi()
    } else {
      setResultSearchData([])
    }
  }, [searchApi, search])

  return {
    resultSearchData
  } as const
}

export { useContent }
