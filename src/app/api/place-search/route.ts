import axios from 'axios'
import { NextRequest, NextResponse } from 'next/server'

export async function GET(request: NextRequest) {
  const apiKey = process.env.NEXT_PUBLIC_API_KEY
  const searchQuery = request.nextUrl.searchParams.get('search')
  const response = await axios.get(
    `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${searchQuery}&key=${apiKey}`
  )
  return NextResponse.json({ result: response.data.results })
}
