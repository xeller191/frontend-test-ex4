import Image from 'next/image'
import { SearchApiResponse } from '../types/saerch-api-response'
type Props = {
  searchData: SearchApiResponse
}
export default function ContentCard({ searchData }: Readonly<Props>) {
  return (
    <div className="flex flex-row items-center justify-center mt-10">
      <div
        className={`min-w-44 h-44 bg-${[
          searchData.icon_background_color
        ]} flex justify-center items-center`}
      >
        <Image
          src={searchData.icon}
          objectFit="contain"
          width={176}
          height={176}
          alt="Picture of Place"
        />
      </div>

      <div className="flex flex-col ml-6 w-4/5">
        <h1 className="font-bold text-2xl">{searchData.name}</h1>
        <h1>{searchData.formatted_address}</h1>
        <br />
        <h1>{searchData.reference}</h1>
      </div>
    </div>
  )
}
