import HeaderSearch from './header-search'
import HeaderTitleName from './header-title-name'

export default function Header() {
  return (
    <div className="  md:flex md:flex-row md:justify-between items-end">
      <HeaderTitleName />
      <HeaderSearch />
    </div>
  )
}
