import { useContent } from '../custom-hook/use-content'
import ContentCard from './content-card'

export default function Content() {
  const { resultSearchData } = useContent()

  return (
    <>
      {resultSearchData.length > 0 ? (
        resultSearchData.map((result) => (
          <ContentCard key={result.place_id} searchData={result} />
        ))
      ) : (
        <div>ไม่พบข้อมูล กรุณาค้นหา</div>
      )}
    </>
  )
}
