import Image from 'next/image'
export default function HeaderTitleName() {
  return (
    <div className="flex flex-row items-center">
      <div className="min-w-32 h-32 bg-red-300 ">
        <Image
          src="https://placehold.co/200x200"
          objectFit="contain"
          width={128}
          height={128}
          alt="Picture of Place"
        />
      </div>
      <h1 className="text-4xl font-bold ml-2">Place Search</h1>
    </div>
  )
}
