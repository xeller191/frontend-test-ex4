import SearchComponent from './search-component'

export default function HeaderSearch() {
  return (
    <div className="mt-2 md:w-80">
      <SearchComponent />
    </div>
  )
}
