import FooterContent from './footer-content'

export default function Footer() {
  return (
    <div className="flex items-end justify-end ">
      <FooterContent />
    </div>
  )
}
