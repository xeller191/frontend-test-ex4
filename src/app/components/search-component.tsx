import { setSearch } from '@/app/lib/features/searchSlic'
import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
export default function SearchComponent() {
  const [searchTerm, setSearchTerm] = useState('')
  const dispatch = useDispatch()

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      dispatch(setSearch(searchTerm))
    }, 1000)
    return () => clearTimeout(timeoutId)
  }, [searchTerm, 1000])

  return (
    <form>
      <div className="flex">
        <div className="relative w-full">
          <input
            type="search"
            id='location-search"'
            className="block p-2.5 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-e-lg border-2 rounded-md border-gray-300  dark:bg-gray-700 dark:border-s-gray-700  dark:border-gray-600 dark:placeholder-gray-400 dark:text-white "
            placeholder="Search"
            required
            onChange={(e) => {
              setSearchTerm(e.target.value)
            }}
          />
          <button
            type="submit"
            className="absolute top-0 end-0 h-full p-2.5 text-sm font-medium text-white bg-blue-700 rounded-e-lg border  "
          >
            <svg
              className="w-4 h-4"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 20 20"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
              />
            </svg>
            <span className="sr-only">Search</span>
          </button>
        </div>
      </div>
    </form>
  )
}
