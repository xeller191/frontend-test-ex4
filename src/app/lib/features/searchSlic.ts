import { createSlice } from '@reduxjs/toolkit'

interface SearchState {
  search: string
}

const initialState: SearchState = {
  search: ''
}

const authSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
    setSearch: (state, { payload }) => {
      state.search = payload
    }
  }
})

export const { setSearch } = authSlice.actions

export default authSlice.reducer
