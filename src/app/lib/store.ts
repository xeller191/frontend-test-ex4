import { configureStore } from '@reduxjs/toolkit'

import searchReducer from './features/searchSlic'
export function makeStore() {
  return configureStore({
    reducer: {
      searchStore: searchReducer
    },
    devTools: true
  })
}

export const store = makeStore()

export type AppStore = ReturnType<typeof makeStore>
export type RootState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']
